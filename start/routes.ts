/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'
// Route.on('/').render("article/view").middleware('auth');
Route.get('/', 'ArticlesController.index').middleware('auth');

Route.resource('news', 'ArticlesController').paramFor('news','slug').middleware({
    create: ["auth"],
    edit: ["auth"],
    store: ["auth"],
    destroy: ["auth"],
});

Route.on('/login').render("auth/login").as('auth.login');

Route.post("/login",async ({ auth, request, response }) => {
    const email = request.input("email");
    const password = request.input("password");

    await auth.use("web").attempt(email, password);

    return response.redirect("/");
})

Route.post("/logout",async ({ auth, response }) => {
    await auth.use("web").logout();

    return response.redirect("/login");
}).as('auth.logout');