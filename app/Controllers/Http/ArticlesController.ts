// import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Article from 'App/Models/Article';
import CreateArticleValidator from 'App/Validators/CreateArticleValidator';
// import Application from '@ioc:Adonis/Core/Application'
import Drive from '@ioc:Adonis/Core/Drive'

export default class ArticlesController {

    public async index({ view }) {
        //fetch data from db
        const articles = await Article.all();
        return view.render('article/view', {articles})
    }

    public create({ view }) {
        return view.render("article/create");
    }

    public async show({ view, params }){
        const article = await Article.findBy('slug', params.slug);
        return view.render("article/show",{article});
    }

    public async store({ response, request }) {
        const payload = await request.validate(CreateArticleValidator);

        if(payload.image){
            await payload.image.moveToDisk('./');
        }
        
        const avatar = await Drive.getUrl(payload.image.fileName);
        payload.image = avatar;
        await Article.create(payload);

        // return response.redirect().back();
        return response.redirect("/");
        
    }

    public async edit({view,params}){
        const article = await Article.findBy('slug', params.slug);
        return view.render("article/edit", { article });
    }

    public async update({ request,response,params }){
        const payload = await request.validate(CreateArticleValidator);
        
        if(payload.image){
            const article = await Article.findBy('slug', params.slug);
            await payload.image.moveToDisk('./');
            if(article){
                let path = await article.image;
                let path2 = path.split('/').pop()
                if(path2){
                    await Drive.delete(path2);
                }
                
            }
            const avatar = await Drive.getUrl(payload.image.fileName);
            payload.image = avatar;
        }
        await Article.query().where('slug', params.slug).update(payload);
        // return response.redirect().back();
        return response.redirect("/");
    }

    public async destroy({ response,params }){
        const article = await Article.findBy('slug', params.slug);
        if(article){
            let path = await article.image;
            let path2 = path.split('/').pop()
            if(path2){
                await Drive.delete(path2);
            }
            article.delete();
            return response.redirect().back();
        }
    }
}
