import { DateTime } from 'luxon'
import { BaseModel, beforeCreate, column } from '@ioc:Adonis/Lucid/Orm'

export default class Article extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public slug: string

  @column()
  public email: string

  @column()
  public image: string

  @column()
  public address: string

  @column()
  public gender: string

  @column()
  public position: string

  @column()
  public contact: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeCreate()
  public static async createSlug(article: Article){
    article.slug = article.$dirty.name.replace(/\s/g, '-') + +new Date();
  }
}
